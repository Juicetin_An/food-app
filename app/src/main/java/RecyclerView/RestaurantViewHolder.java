package RecyclerView;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;

public class RestaurantViewHolder extends RecyclerView.ViewHolder {


    //Constructor
    public RestaurantViewHolder(View itemView) {
        super(itemView);


        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.imageButton_view);
        restaurantNameTextView = itemView.findViewById(R.id.restaurant_name);
        ratingTextView = itemView.findViewById(R.id.rating);
        orderTimeTextView = itemView.findViewById(R.id.order_time);

    }



        //Properties
    View itemView;
    ImageView imageView;
    TextView restaurantNameTextView;
    TextView ratingTextView;
    TextView orderTimeTextView;

}
