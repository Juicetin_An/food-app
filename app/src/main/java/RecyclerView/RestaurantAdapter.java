package RecyclerView;

import android.content.Context;
import android.content.RestrictionEntry;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;

import java.util.ArrayList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

    RestaurantAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> restaurants) {
        this.context = context;
        this.restaurants = restaurants;
    }
    //Overridden Methods
    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_restaurant, parent, false);
        RestaurantViewHolder viewHolder = new RestaurantViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, final int position) {
        Restaurant restaurant = restaurants.get(position);
        holder.imageView.setImageResource(restaurant.imageResource);
        holder.restaurantNameTextView.setText(restaurant.restaurantName);
        holder.ratingTextView.setText(restaurant.rating);
        holder.orderTimeTextView.setText(restaurant.orderTime);
    }




    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    //Properties
    Context context;
    ArrayList<Restaurant> restaurants;


}
