package com.example.foodapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Landing_Page extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing__page);


        ImageButton icecream = findViewById(R.id.icecreamimage);
        icecream.setOnClickListener(this::helpsadness);

        ImageButton chinesefood = findViewById(R.id.ChineseFoodImage);
        chinesefood.setOnClickListener(this::helpsadness);

        ImageButton americanfood = findViewById(R.id.AmericanFoodImage);
        americanfood.setOnClickListener(this::helpsadness);

        ImageButton fastFood = findViewById(R.id.FastFoodImage);
        fastFood.setOnClickListener(this::helpsadness);

        ImageButton checkoutpage = findViewById(R.id.checkoutbutton);
        checkoutpage.setOnClickListener(this::ghgh);

    }


    void helpsadness(View view) {
        Intent intent = new Intent(Landing_Page.this, restaurant_list.class); //change back to restaurant_list
        startActivity(intent);
        finish();
    }

    void ghgh(View view) {
        Intent intent = new Intent(Landing_Page.this, Checkout_Page.class);
        startActivity(intent);
        finish();
    }

}