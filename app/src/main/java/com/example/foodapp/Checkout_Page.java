package com.example.foodapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.foodapp.Common.Common;
import com.example.foodapp.Database.Database;
import com.example.foodapp.Model.Order;
import com.example.foodapp.Model.Request;
import com.example.foodapp.ViewHolder.CartAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Checkout_Page extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference requests;

    TextView txtTotalPrice;
    Button btnPlace;

    List<Order> cart = new ArrayList<>();

    CartAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_page);

        //Firebase
        database = FirebaseDatabase.getInstance();
        requests=database.getReference("Request");

        //Init
        recyclerView = (RecyclerView)findViewById(R.id.listCart);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        txtTotalPrice = (TextView)findViewById(R.id.total);
        btnPlace = (Button)findViewById(R.id.btnPlaceOrder);

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAlertDialog();
            }
        });

        loadListFood();


    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Checkout_Page.this);
        alertDialog.setTitle("One More step!");
        alertDialog.setMessage("Enter your address: ");

        final EditText edtAddress = new EditText(Checkout_Page.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        edtAddress.setLayoutParams(lp);
        alertDialog.setView(edtAddress); //Add edit Text to alert Dialog
        alertDialog.setIcon(R.drawable.ic_baseline_shopping_cart_24);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Create new Argument
                Request request = new Request(
                        Common.currentUser.getPhone(),
                        Common.currentUser.getName(),
                        edtAddress.getText().toString(),
                        txtTotalPrice.getText().toString(),
                        cart
                );

                //Submit to Firebase
                //use System.CurrentMilli to key
                requests.child(String.valueOf(System.currentTimeMillis()))
                        .setValue(request);
                //Delete Cart
                new Database(getBaseContext()).cleanCart();
                Toast.makeText(Checkout_Page.this, "Thank you", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }

    private void loadListFood() {
        cart = new Database(this).getCarts();
        adapter = new CartAdapter(cart,this);
        recyclerView.setAdapter(adapter);

        //Calculate total price
        int total = 0;
        for (Order order:cart)
            total += (Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
        Locale locale = new Locale("en", "CA");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        txtTotalPrice.setText(fmt.format(total));
    }
}

//package com.example.foodapp;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import java.util.ArrayList;
//
//public class Checkout_Page extends AppCompatActivity {
//
//
//    // Singleton
//    public static Checkout_Page getInstance() {
//        if (sharedInstance == null) {
//            sharedInstance = new Checkout_Page();
//        }
//        return sharedInstance;
//    }
//    private static Checkout_Page sharedInstance = null;
//    private Checkout_Page() {}  // Don't allow instantiation outside of this class
//
//    // Public methods
//    public Integer numberOfItems() {
//        Integer numberOfItems = 0;
//        for (CartItem cartItem : cartItems) {
//            numberOfItems += cartItem.getQuantity();
//        }
//        return numberOfItems;
//    }
//
//    public Integer totalPriceInCents() {
//        Integer totalPriceInCents = 0;
//        for (CartItem cartItem : cartItems) {
//            totalPriceInCents += cartItem.subtotalPriceInCents();
//        }
//        return totalPriceInCents;
//    }
//
//
//    public ArrayList<CartItem> getCartItems() {
//        return cartItems;
//    }
//
//    public void clear() {
//        cartItems = new ArrayList<>();
//    }
//
//    // Private properties
//    private ArrayList<CartItem> cartItems = new ArrayList<>();
//
//    // Nested classes
//    public class CartItem {
//
//        // Public methods
//        public Integer getQuantity() {
//            return quantity;
//        }
//
//        public Integer itemPriceInCents() {
//            return dish == null ? 0 : Integer.valueOf(R.id.priceincents);
//        }
//
//        public String dishName() {
//            return dish == null ? "" : String.valueOf(R.id.itemname);
//        }
//
//        public Integer subtotalPriceInCents() {
//            return quantity * itemPriceInCents();
//        }
//
//        // Constructor
//        public CartItem(orderpage dish, Integer quantity) {
//            this.dish = dish;
//            this.quantity = quantity;
//        }
//
//        // Private properties
//        private orderpage dish;
//        private Integer quantity;
//    }
//}