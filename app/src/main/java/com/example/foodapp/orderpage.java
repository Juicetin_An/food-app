//package com.example.foodapp;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.os.Bundle;
//
//public class orderpage extends AppCompatActivity {
//
//    @SuppressLint("SetTextI18n")
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_orderpage);
//
//
//        //find items
//        View orderpageitem1 = findViewById(R.id.orderpageitem1);
//        View orderpageitem2 = findViewById(R.id.orderpageitem2);
//        View orderpageitem3 = findViewById(R.id.orderpageitem3);
//        View orderpageitem4 = findViewById(R.id.orderpageitem4);
//
//        //find texts
//        TextView itemname1;
//        itemname1 = orderpageitem1.findViewById(R.id.itemname);
//        TextView itemname2;
//        itemname2 = orderpageitem2.findViewById(R.id.itemname);
//        TextView itemname3;
//        itemname3 = orderpageitem3.findViewById(R.id.itemname);
//        TextView itemname4;
//        itemname4 = orderpageitem4.findViewById(R.id.itemname);
//        //set texts
//        itemname1.setText("Legendary burger");
//        itemname2.setText("Chicken burger");
//        itemname3.setText("Grilled eggs benedict");
//        itemname4.setText("Big boi burger");
//
//        TextView ingredient1;
//        ingredient1 = orderpageitem1.findViewById(R.id.ingredients);
//        TextView ingredient2;
//        ingredient2 = orderpageitem2.findViewById(R.id.ingredients);
//        TextView ingredient3;
//        ingredient3 = orderpageitem3.findViewById(R.id.ingredients);
//        TextView ingredient4;
//        ingredient4 = orderpageitem4.findViewById(R.id.ingredients);
//        //set texts
//        ingredient1.setText("Lettuce, tomato, sauce, meat, cheese, pickles, bun");
//        ingredient2.setText("Chicken, meat, lettuce, bun, sesame seeds, sauce");
//        ingredient3.setText("Eggs, meat, english muffin, sauce, tater tots, fork");
//        ingredient4.setText("6 patties, 2 buns, fist of cheese, bucket of fries,");
//
//
//    }
//}

package com.example.foodapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class orderpage extends AppCompatActivity {

    public String itemname;
    public String ingredients;
    public Integer priceincents;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderpage);
        //find items
        View orderpageitem1 = findViewById(R.id.orderpageitem1);
        View orderpageitem2 = findViewById(R.id.orderpageitem2);
        View orderpageitem3 = findViewById(R.id.orderpageitem3);
        View orderpageitem4 = findViewById(R.id.orderpageitem4);
        View orderpageitem5 = findViewById(R.id.orderpageitem5);
        View orderpageitem6 = findViewById(R.id.orderpageitem6);
        View orderpageitem7 = findViewById(R.id.orderpageitem7);
        View orderpageitem8 = findViewById(R.id.orderpageitem8);

        //find texts


        TextView itemname1;
        itemname1 = orderpageitem1.findViewById(R.id.itemname);
        TextView itemname2;
        itemname2 = orderpageitem2.findViewById(R.id.itemname);
        TextView itemname3;
        itemname3 = orderpageitem3.findViewById(R.id.itemname);
        TextView itemname4;
        itemname4 = orderpageitem4.findViewById(R.id.itemname);
        TextView itemname5;
        itemname5 = orderpageitem5.findViewById(R.id.itemname);
        TextView itemname6;
        itemname6 = orderpageitem6.findViewById(R.id.itemname);
        TextView itemname7;
        itemname7 = orderpageitem7.findViewById(R.id.itemname);
        TextView itemname8;
        itemname8 = orderpageitem8.findViewById(R.id.itemname);
        //set texts
        itemname1.setText("Legendary burger");
        itemname2.setText("Chicken burger");
        itemname3.setText("Grilled eggs benedict");
        itemname4.setText("Big boi burger");
        itemname5.setText("Legendary burger");
        itemname6.setText("Chicken burger");
        itemname7.setText("Grilled eggs benedict");
        itemname8.setText("Big boi burger");

        TextView ingredient1;
        ingredient1 = orderpageitem1.findViewById(R.id.ingredients);
        TextView ingredient2;
        ingredient2 = orderpageitem2.findViewById(R.id.ingredients);
        TextView ingredient3;
        ingredient3 = orderpageitem3.findViewById(R.id.ingredients);
        TextView ingredient4;
        ingredient4 = orderpageitem4.findViewById(R.id.ingredients);
        TextView ingredient5;
        ingredient5 = orderpageitem1.findViewById(R.id.ingredients);
        TextView ingredient6;
        ingredient6 = orderpageitem2.findViewById(R.id.ingredients);
        TextView ingredient7;
        ingredient7 = orderpageitem3.findViewById(R.id.ingredients);
        TextView ingredient8;
        ingredient8 = orderpageitem4.findViewById(R.id.ingredients);
        //set texts
        ingredient1.setText("Lettuce, tomato, sauce, meat, cheese, pickles, bun");
        ingredient2.setText("Chicken, meat, lettuce, bun, sesame seeds, sauce");
        ingredient3.setText("Eggs, meat, english muffin, sauce, tater tots, fork");
        ingredient4.setText("6 patties, 2 buns, fist of cheese, bucket of fries,");
        ingredient5.setText("Lettuce, tomato, sauce, meat, cheese, pickles, bun");
        ingredient6.setText("Chicken, meat, lettuce, bun, sesame seeds, sauce");
        ingredient7.setText("Eggs, meat, english muffin, sauce, tater tots, fork");
        ingredient8.setText("6 patties, 2 buns, fist of cheese, bucket of fries,");





//other stuff start
        TextView priceincent1;
        priceincent1 = orderpageitem1.findViewById(R.id.priceincents);
        TextView priceincent2;
        priceincent2 = orderpageitem2.findViewById(R.id.priceincents);
        TextView priceincent3;
        priceincent3 = orderpageitem3.findViewById(R.id.priceincents);
        TextView priceincent4;
        priceincent4 = orderpageitem4.findViewById(R.id.priceincents);
        TextView priceincent5;
        priceincent5 = orderpageitem1.findViewById(R.id.priceincents);
        TextView priceincent6;
        priceincent6 = orderpageitem2.findViewById(R.id.priceincents);
        TextView priceincent7;
        priceincent7 = orderpageitem3.findViewById(R.id.priceincents);
        TextView priceincent8;
        priceincent8 = orderpageitem4.findViewById(R.id.priceincents);
        //set texts
        priceincent1.setText(Integer.toString(10));
        priceincent2.setText(Integer.toString(10));
        priceincent3.setText(Integer.toString(10));
        priceincent4.setText(Integer.toString(10));
        priceincent5.setText(Integer.toString(10));
        priceincent6.setText(Integer.toString(10));
        priceincent7.setText(Integer.toString(10));
        priceincent8.setText(Integer.toString(10));

//other stuff end



        ImageButton orderpagebutton1 = findViewById(R.id.orderpageitem1);
        orderpagebutton1.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton2 = findViewById(R.id.orderpageitem2);
        orderpagebutton2.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton3 = findViewById(R.id.orderpageitem3);
        orderpagebutton3.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton4 = findViewById(R.id.orderpageitem4);
        orderpagebutton4.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton5 = findViewById(R.id.orderpageitem5);
        orderpagebutton5.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton6 = findViewById(R.id.orderpageitem6);
        orderpagebutton6.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton7 = findViewById(R.id.orderpageitem7);
        orderpagebutton7.setOnClickListener(this::helpsadness);
        ImageButton orderpagebutton8 = findViewById(R.id.orderpageitem8);
        orderpagebutton8.setOnClickListener(this::helpsadness);
        ImageButton backbutton = findViewById(R.id.backButton);
        backbutton.setOnClickListener(this::meep);

        ImageButton checkoutpage = findViewById(R.id.checkoutbutton);
        checkoutpage.setOnClickListener(this::helpsadness);

    }


    void helpsadness(View view) {
        Intent intent = new Intent(this, Checkout_Page.class);
        startActivity(intent);
        finish();


    }
    void meep(View view) {
        Intent intent = new Intent(this, restaurant_list.class);
        startActivity(intent);
        finish();


    }

}